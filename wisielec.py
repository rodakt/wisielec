#!/usr/bin/env python3

from time import sleep
from losowe_haslo import losuj_hasło
from ascii_art import rysunek
from komunikaty import Komunikaty
import json

PODKREŚLNIK = '_'
ODSTĘP = '\n\n'
MAKS_PRÓB = 10

def koduj_hasło(hasło, wykorzystane_litery, litery):
    '''Zwraca sformatowane hasło z podkreślnikami
    w miejscu niewykorzystanych liter.'''
    zakodowane_hasło = []

    for znak in hasło:
        if (znak not in litery) or (znak in wykorzystane_litery):
            kod = znak
        else:
            kod = PODKREŚLNIK

        zakodowane_hasło.append(kod)

    return ' '.join(zakodowane_hasło)


def pokaż_dostępne_litery(wykorzystane_litery, litery):
    '''Łańcuch niewykorzystanych liter.'''
    return ' '.join(lit for lit in litery if lit not in wykorzystane_litery)


def szubienica(pozostało_liter,
               pozostało_prób,
               dostępne_litery,
               zakodowane_hasło,
               komunikaty,
               runda):
    '''Wyświetla stan gry.'''
    print(ODSTĘP)
    print(f'{komunikaty.runda()}: {runda}.',
          f'{komunikaty.pozostało_liter()}: {pozostało_liter}.', 
          f'{komunikaty.pozostało_prób()}: {pozostało_prób}.')
    print(komunikaty.dostępne_litery())
    print(dostępne_litery)
    print()
    print(zakodowane_hasło)
    print()
    print(rysunek[pozostało_prób])
    print(komunikaty.twoja_litera(), end='')


def ostrzeżenie(tekst, znaczek='#', sec=2):
    d = min(80, len(tekst))
    print()
    print(d * znaczek)
    print(tekst)
    print(d * znaczek)
    print()
    sleep(sec)


def koniec_gry(zakodowane_hasło,
               adres_url,
               komunikaty,
               pozostało_prób,
               runda):
    '''Komunikaty na koniec gry.'''
    długość = max(len(zakodowane_hasło), len(adres_url))
    print(ODSTĘP)
    print(f' {(długość+2) * "-"} ')
    print(f'| {zakodowane_hasło:<{długość}} |')
    print(f'| {adres_url:<{długość}} |')
    print(f' {(długość+2) * "-"} ')
    punkty = 5 * pozostało_prób + (len(komunikaty.zwróć_litery()) - runda)
    if pozostało_prób > 0:
        print(komunikaty.wygrana())
        punkty += 50
    else:
        print(rysunek[pozostało_prób])
        print(komunikaty.przegrana())
    print(f'{komunikaty.punktacja()}: {punkty}')
    print(ODSTĘP)
    hall_of_fame(punkty, runda, zakodowane_hasło, komunikaty)


def hall_of_fame(punkty, runda, zakodowane_hasło, komunikaty):
    '''Funkcja zapisująca do pliku najlepsze wyniki.'''
    imie = input(f'{komunikaty.podaj_imie()}: ')
    
    try:
        with open('hall_of_fame.json', 'r') as plik:
            hall_of_fame = json.load(plik)
        for index, wpis in enumerate(hall_of_fame):
            if wpis[1] < punkty:
                hall_of_fame.insert(index,
                                    [imie, punkty, runda, zakodowane_hasło])
                break
        else:
            hall_of_fame.insert(len(hall_of_fame),
                                [imie, punkty, runda, zakodowane_hasło])
            
    except FileNotFoundError:
        hall_of_fame = list([[imie, punkty, runda, zakodowane_hasło]])
        
    for element in hall_of_fame:
        print(element)
        
    with open('hall_of_fame.json', 'w') as plik:
        json.dump(hall_of_fame, plik)        


def wisielec():
    '''Gra w Wisielca na podstawie tytułu
    losowego hasła z Wikipedii.'''
    komunikaty = Komunikaty()
    print(komunikaty.oczekiwanie_na_hasło())
    hasło, adres_url = losuj_hasło(komunikaty.zwróć_język())
    hasło = hasło.upper()
    liczba_błędów = 0
    pozostało_prób = MAKS_PRÓB
    wykorzystane_litery = ''
    dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery, komunikaty.zwróć_litery())
    zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery, komunikaty.zwróć_litery())
    pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)
    runda = 1

    while True:
        # Pokaż stan i pobierz literę.
        while True:
            szubienica(pozostało_liter,
                       pozostało_prób,
                       dostępne_litery,
                       zakodowane_hasło,
                       komunikaty,
                       runda)
            litera = input().upper()

            if len(litera) != 1:
                ostrzeżenie(komunikaty.tylko_jedna_litera())
                continue

            if litera not in komunikaty.zwróć_litery():
                ostrzeżenie(komunikaty.wymaganie_litery())
                continue

            if litera in wykorzystane_litery:
                ostrzeżenie(komunikaty.powtarzalność_liter())
                continue

            break

        runda += 1
        wykorzystane_litery += litera
        dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery, komunikaty.zwróć_litery())

        if litera in hasło:
            zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery, komunikaty.zwróć_litery())
            pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)

            if pozostało_liter == 0:  # Wygrana.
                koniec_gry(zakodowane_hasło,
                           adres_url,
                           komunikaty,
                           pozostało_prób,
                           runda)
                return

        else:
            liczba_błędów += 1
            pozostało_prób -= 1

            if pozostało_prób == 0:  # Przegrana.
                koniec_gry(koduj_hasło(hasło, komunikaty.zwróć_litery(), komunikaty.zwróć_litery()),
                           adres_url,
                           komunikaty,
                           pozostało_prób,
                           runda)
                return


if __name__ == '__main__':
    wisielec()
