import json

class Komunikaty():
    '''Klasa odpowiadająca za komunikaty do użytkownika oraz wybór przez użytkownika języka tych komunikatów.
    Korzysta z komunikatów zapisanych w pliku wersje_jezykowe.json.
    
    Metody:
    zwróć_język()
    zwróć_litery()
    pozostało_liter()
    pozostało_prób()
    dostępne_litery()
    twoja_litera()
    oczekiwanie_na_hasło()
    tylko_jedna_litera()
    wymaganie_liter()
    powtarzalność_liter()
    wygrana()
    przegrana()
    runda()
    punktacja()
    podaj_imie()
    '''
    def __init__(self):
        '''Inicjalizacja instancji klasy. Dodaje atrybuty prywatne self._języki i self._komunikaty,
        ustalając je odpowiednio na wartość zwróconą przez metodę self._wczytaj_wersje_jezykowe()
        i odpowiedni słownik komunikatów.'''
        self._języki = self._wczytaj_wersje_jezykowe()
        self._komunikaty = self._języki[self._wybierz_język()]
    
    def _wczytaj_wersje_jezykowe(self):
        '''Wczytuje plik wersje_jezykowe.json i zwraca przechowywany tam obiekt.'''
        with open('wersje_jezykowe.json', 'r') as plik:
            języki = json.load(plik)
            
        return języki
    
    def _wybierz_język(self):
        '''Pyta użytkownika o wybranie języka spośród znajdujących się w pliku wersje_jezykowe.json.
        Po trzech nieudanych próbach wybiera język domyślny - polski.
        Zwraca wybrany język.'''
        for _ in range(3):
            język = str(input(f'Wybierz język {tuple(self._języki.keys())}: ')).lower()
            if język not in self._języki.keys():
                print('Błędny język.')
            else:
                break
        else:
            print('Nie wybrano prawidłowego języka. Gra uruchomi się w języku domyślnym - polskim.')
            język = 'polski'
        
        return język
    
    def zwróć_język(self):
        return self._komunikaty['język']
    
    def zwróć_litery(self):
        return self._komunikaty['litery']
    
    def pozostało_liter(self):
        return self._komunikaty['Pozostało liter']
    
    def pozostało_prób(self):
        return self._komunikaty['Pozostało prób']
    
    def dostępne_litery(self):
        return self._komunikaty['Dostępne litery:']
    
    def twoja_litera(self):
        return self._komunikaty['Twoja litera: ']
    
    def oczekiwanie_na_hasło(self):
        return self._komunikaty['Szukam hasła, proszę czekać... :-)']
    
    def tylko_jedna_litera(self):
        return self._komunikaty['Możesz podać tylko jedną literę!']
    
    def wymaganie_litery(self):
        return self._komunikaty['Podaj LITERĘ!']
    
    def powtarzalność_liter(self):
        return self._komunikaty['Ta litera już była!']
    
    def wygrana(self):
        return self._komunikaty['Brawo! Wygrałeś!']
    
    def przegrana(self):
        return self._komunikaty['Wisisz!']

    def runda(self):
        return self._komunikaty['Runda']

    def punktacja(self):
        return self._komunikaty['Zdobyte punkty']

    def podaj_imie(self):
        return self._komunikaty['Podaj swoje imie']
