# Wartości stałe wyniesione do oddzielnego pliku, aby przy zmianach API nie trzeba było robić zmian w całym projekcie.

# Końcówka API dla polskiej Wikipedii.
API_URL_PL = 'https://pl.wikipedia.org/w/api.php'
WIKI_URL_PL = 'https://pl.wikipedia.org'

# Końcówka API dla angielskiej Wikipedii.
API_URL_EN = 'https://en.wikipedia.org/w/api.php'
WIKI_URL_EN = 'https://en.wikipedia.org'