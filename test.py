import unittest
from time import perf_counter
from unittest.mock import patch, call, Mock
from komunikaty import Komunikaty
from wisielec import koduj_hasło, pokaż_dostępne_litery, ostrzeżenie
litery = 'ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻ'

class Komunikaty_Test(unittest.TestCase):

    @patch('komunikaty.Komunikaty._wybierz_język')
    def setUp(self, mock_wybierz_język):
        '''Ustala wartość zwracaną dla funkcji wybierz_język na 'polski'.'''
        self._wybierz_język = mock_wybierz_język
        self._wybierz_język.return_value = 'polski'
        self.k = Komunikaty()

    def test_sprawdź_docstring(self,):
        assert Komunikaty.__doc__

    def test_zwróć_język(self):
        self.assertEqual(self.k.zwróć_język(), 'polski')

    def test_zwróć_litery(self):
        self.assertEqual(self.k.zwróć_litery(), litery)

    def test_pozostało_liter(self):
        self.assertEqual(self.k.pozostało_liter(), 'Pozostało liter')

    def test_pozostało_prób(self):
        self.assertEqual(self.k.pozostało_prób(), 'Pozostało prób')

    def test_dostępne_litery(self):
        self.assertEqual(self.k.dostępne_litery(), 'Dostępne litery:')

    def test_twoja_litera(self):
        self.assertEqual(self.k.twoja_litera(), 'Twoja litera: ')

    def test_oczekiwanie_na_hasło(self):
        self.assertEqual(self.k.oczekiwanie_na_hasło(), 'Szukam hasła, proszę czekać... :-)')

    def test_tylko_jedna_litera(self):
        self.assertEqual(self.k.tylko_jedna_litera(), 'Możesz podać tylko jedną literę!')

    def test_wymaganie_litery(self):
        self.assertEqual(self.k.wymaganie_litery(), 'Podaj LITERĘ!')

    def test_powtarzalność_liter(self):
        self.assertEqual(self.k.powtarzalność_liter(), 'Ta litera już była!')

    def test_wygrana(self):
        self.assertEqual(self.k.wygrana(), 'Brawo! Wygrałeś!')

    def test_przegrana(self):
        self.assertEqual(self.k.przegrana(), 'Wisisz!')

class Koduj_hasło_Test(unittest.TestCase):
    def test_sprawdź_docstring(self):
        assert koduj_hasło.__doc__
    
    def test_wersja_początkowa(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "", litery),
                         "_ _ _   _ _   _ _ _ _")
        
    def test_brak_zmian(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA",
                                     "BCDEFGHIJNPQRSUVWXYZĄĆĘŁŃÓŚŹŻ", litery),
                         "_ _ _   _ _   _ _ _ _")
        
    def test_dodana_litera(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "A", litery),
                         "A _ A   _ A   _ _ _ A")
        
    def test_dodane_dwie_litery(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "AO", litery),
                         "A _ A   _ A   _ O _ A")
        
    def test_dodane_trzy_litery(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "AOL", litery),
                         "A L A   _ A   _ O _ A")
        
    def test_dodane_cztery_litery(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "AOLM", litery),
                         "A L A   M A   _ O _ A")
        
    def test_dodane_pięć_liter(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "AOLMK", litery),
                         "A L A   M A   K O _ A")
        
    def test_dodane_sześć_liter(self):
        self.assertEqual(koduj_hasło("ALA MA KOTA", "AOLMKT", litery),
                         "A L A   M A   K O T A")
        
    def test_puste(self):
        self.assertEqual(koduj_hasło("", "", litery), "")

class Pokaż_dostępne_litery_Test(unittest.TestCase):
    def test_sprawdź_docstring(self):
        assert pokaż_dostępne_litery.__doc__
        
    def test_puste(self):
        self.assertEqual(pokaż_dostępne_litery("", litery),
                         " ".join(litery))
    
    def test_jedna_litera(self):
        self.assertEqual(pokaż_dostępne_litery("A", litery),
                         " ".join(litery[1:]))
    
    def test_dwie_litery(self):
        self.assertEqual(pokaż_dostępne_litery("AŻ", litery),
                         " ".join(litery[1:-1]))
    
    def test_trzy_litery(self):
        self.assertEqual(pokaż_dostępne_litery("CAB", litery),
                         " ".join(litery[3:]))
    
    def test_powtarzające_się_litery(self):
        self.assertEqual(pokaż_dostępne_litery("AAABCC", litery),
                         " ".join(litery[3:]))
    
    def test_wszystkie_litery(self):
        self.assertEqual(pokaż_dostępne_litery(litery, litery), "")
        
        
class Ostrzeżenia_Test(unittest.TestCase):
    def test_sleep(self):
        with patch('builtins.print') as mock_print:
            start = perf_counter()
            ostrzeżenie('sleep')
            end = perf_counter()
        self.assertEqual(round(end - start, 0), 2.0)
        
    def test_puste(self):
        with patch('builtins.print') as mock_print:
            ostrzeżenie('', sec = 0)
        
        self.assertEqual(mock_print.mock_calls,
                         [call(), call(''), call(''), call(''), call()])
        
    def test_jeden(self):
        with patch('builtins.print') as mock_print:
            ostrzeżenie('1', sec = 0)
        
        self.assertEqual(mock_print.mock_calls,
                         [call(), call('#'), call('1'), call('#'), call()])
        
    def test_dwa(self):
        with patch('builtins.print') as mock_print:
            ostrzeżenie('12', sec = 0)
            
        self.assertEqual(mock_print.mock_calls,
                         [call(), call('##'), call('12'), call('##'), call()])
        
    def test_długie(self):
        with patch('builtins.print') as mock_print:
            ostrzeżenie('1234567890' * 10, sec = 0)
            
        self.assertEqual(mock_print.mock_calls,
                         [call(),
                          call('#' * 80),
                          call('1234567890' * 10),
                          call('#' * 80),
                          call()])

if __name__ == '__main__':
    unittest.main()
