## Opis

Wersja konsolowa gry w [Wisielca](https://pl.wikipedia.org/wiki/Wisielec_(gra)).

## ToDo

1. Testy, testy, testy, ...
2. Szubienica w ASCII-Art jako dodatek do punktacji.
3. Rundy gry, punktacja, Hall of Fame.
4. Wersja wielojęzyczna.
